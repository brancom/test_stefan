<?php

namespace App\Http\Controllers;

use App\Files;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Mail\FileUpload;
use Illuminate\Support\Facades\Mail;



class FileController extends Controller
{
    public function show()
    {
        return view('welcome');
    }
    public function store(Request $request, Mail $mail)
    {
        $data['mail-sender'] = $request['mail-sender'];
        $data['mail-receiver'] = $request['mail-receiver'];
        $data['file'] = $request['file'];
        $data['token'] = Str::random(12);

        $file = new Files;
        $file->mail_sender = $data['mail-sender'];
        $file->mail_receiver = $data['mail-receiver'];
        $file->file = $data['file'];
        $file->token = $data['token'];
        $file->save();

        $sender   = $data['mail-sender'];
        $receiver = $data['mail-receiver'];
        $token    = $data['token'];

        $to = $receiver;
        $subject = 'Retreive your file with the code.';
        $message = $token;
        $headers = "From:" . $sender;
        mail($to,$subject,$message, $headers);
        echo "Test email sent";

        return redirect('/');
    }

    public function search()
    {
        $search = $_GET['search'];

        $token = DB::table('files')->where('token', '=', $search)->get('file');

        $token = $token['0']->file;

        return view('file', compact('token'));
    }
}
