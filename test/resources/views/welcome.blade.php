 @include('templates.navbar')
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    File Upload
                </div>

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <form method="post" action="/upload">
                    @csrf
                    <div class="form-group">
                        <label for="mail-sender">Email Sender</label>
                        <input name="mail-sender" type="email" class="form-control" id="email-form-send" placeholder="Enter your email">
                    </div>
                    <div class="form-group">
                        <label for="mail-receiver">Email Receiver</label>
                        <input name="mail-receiver" type="mail-receiver" class="form-control" id="mail-receiver" placeholder="Recipient Email">
                    </div>
                    <div class="form-group">
                        <label for="file">Upload File</label>
                        <input name="file" type="file" class="form-control-file" id="File">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </body>
</html>
