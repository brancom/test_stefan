<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>
<nav class="navbar navbar-light bg-dark">
    <a class="navbar-brand" href="/">
        <img src="/images/logo.svg" max-width="225px" height="30" class="d-inline-block align-top" alt="">
    </a>
    <form method="get" action="/search" class="form-inline">
        @csrf
        <input name="search" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-light btn-outline my-2 my-sm-0" type="submit">Search</button>
    </form>
</nav>
